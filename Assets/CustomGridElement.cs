﻿using UnityEngine;
using System.Collections;

public class CustomGridElement : MonoBehaviour {

	public enum Element{road,ground,na};
	public Element ele=Element.na;

	public bool accessible = true;
	public int rowInd, colInd;

	private Material def_mat;
	private bool columnUpdated=false;		//update column only once on mouse enter

	void Start()
	{
		def_mat = GetComponent<MeshRenderer> ().material;
	}

//	void OnMouseDown()
//	{
//		//Debug.Log (transform.name);
//		if (CustomLevelBtnActn.Instance.selectedMat != null) {
//			
//			GetComponent<MeshRenderer> ().material = CustomLevelBtnActn.Instance.selectedMat;
//			accessible = false;
//
//			for (int i = colInd - 1; i >= 0; i--) {
//				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<MeshRenderer> ().material = CustomLevelBtnActn.Instance.ground_mat;
//			}
//			for (int i = colInd + 1; i < LevelCreatorGrid.Instance.columns; i++) {
//				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<MeshRenderer> ().material = def_mat;
//			}
//		}
//	}

	void OnMouseOver()
	{
		if (CustomLevelBtnActn.Instance.selectedMat != null && Input.GetMouseButton (0) && !columnUpdated) {//Debug.Log ("updating column");

			GetComponent<MeshRenderer> ().material = CustomLevelBtnActn.Instance.selectedMat;
			accessible = false;
			ele = Element.road;

			for (int i = colInd - 1; i >= 0; i--) {
				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<MeshRenderer> ().material = CustomLevelBtnActn.Instance.ground_mat;
				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<CustomGridElement> ().ele = Element.ground;
			}
			for (int i = colInd + 1; i < LevelCreatorGrid.Instance.columns; i++) {
				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<MeshRenderer> ().material = def_mat;
				LevelCreatorGrid.gridArray [rowInd, i].GetComponent<CustomGridElement> ().ele = Element.na;
			}

			columnUpdated = true;
		} 
	}

	void OnMouseExit()
	{
		columnUpdated = false;
	}
}
