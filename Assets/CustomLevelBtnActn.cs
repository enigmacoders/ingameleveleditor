﻿using UnityEngine;
using System.Collections;

public class CustomLevelBtnActn : MonoBehaviour {

	public static CustomLevelBtnActn Instance;

	public GameObject roadBlock;
	public GameObject groundBlock;

	public Material road_mat;
	public Material ground_mat;

	[HideInInspector]
	public GameObject selectedPrefab;
	[HideInInspector]
	public Material selectedMat;

	void Start()
	{
		Instance = this;
	}

	public void SelectRoadBlock()
	{
		//selectedPrefab = roadBlock;
		selectedMat=road_mat;
	}

	public void ResetCustomGrid()
	{
		LevelCreatorGrid.Instance.ResetGrid ();
	}

	//save the currently created level on grid
	public void SaveCustomLevel()
	{
		CustomLevelHandler.Instance.SaveCurrCustomLevel ();
	}

	//loads the custom level on grid
	public void LoadCustomLevel()
	{
		CustomLevelHandler.Instance.LoadCustomLevel ();
		LevelCreatorGrid.Instance.gridParent.SetActive (false);
	}

	//view the currently edited level
	public void ViewCustomLevel()
	{
		LevelCreatorGrid.Instance.gridParent.SetActive (false);
		CustomLevelHandler.Instance.CreateCustomLevel ();
	}

	//get bakc to the editing mode on grid
	public void EditCustomLevel()
	{
		CustomLevelHandler.Instance.ClearCustomLevel ();
		LevelCreatorGrid.Instance.gridParent.SetActive (true);
	}
}
