﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System.IO;

public class CustomLevelHandler : MonoBehaviour {

	public static CustomLevelHandler Instance;

	public GameObject customLevelParent;
	public GameObject roadBlockPrefab;
	public GameObject roadSlantPrefab;
	public GameObject groundBlockPrefab;

	public float textureToObjectScale=1;

	private List<double> posX, posY;
	private string customLevelName = "custom1";
	private string customLevelPath;

	void Start()
	{
		Instance = this;
		posX = new List<double> ();
		posY = new List<double> ();
		customLevelPath = Application.persistentDataPath + "/CustomLevels";
		//Debug.Log (customLevelPath);
	}

	public void CreateCustomLevel()
	{
		ReadCustomGrid ();
		InstantiateCustomLevelObjects ();

	}

	public void ReadCustomGrid()
	{
		posX.Clear ();
		posY.Clear ();

		foreach (GameObject go in LevelCreatorGrid.gridArray) {
			if (go.GetComponent<CustomGridElement> ().ele == CustomGridElement.Element.road) {
				posX.Add (go.transform.position.x);
				posY.Add (go.transform.position.y);
			}
		}
	}

	public void ClearCustomLevel()
	{
		for(int i=0;i<customLevelParent.transform.childCount;i++)
			Destroy (customLevelParent.transform.GetChild (i).gameObject);
	}

	public void SaveCurrCustomLevel(string lvlName="custom1")
	{
		if (File.Exists (customLevelPath)) {
			JsonData jsonString1 = JsonMapper.ToJson (posX);
			JsonData jsonString2 = JsonMapper.ToJson (posY);
			File.WriteAllText (customLevelPath + "/" + lvlName + ".json", jsonString1.ToString () +
				"\n" + jsonString2.ToString ());
			Debug.Log (customLevelName + " saved.");
		} else {
			Directory.CreateDirectory (customLevelPath);

			JsonData jsonString1 = JsonMapper.ToJson (posX);
			JsonData jsonString2 = JsonMapper.ToJson (posY);
			File.WriteAllText (customLevelPath + "/" + lvlName + ".json", jsonString1.ToString () +
				"\n" + jsonString2.ToString ());
			Debug.Log (customLevelName + " saved.");
		}
	}

	public void LoadCustomLevel(string lvlName="custom1")
	{
		string jsonString = File.ReadAllText (customLevelPath + "/" + lvlName + ".json");
		string[] jsonArr = jsonString.Split ('\n');

		JsonData jxPos = JsonMapper.ToObject (jsonArr [0]);
		JsonData jyPos = JsonMapper.ToObject (jsonArr [1]);

		posX.Clear ();
		posY.Clear ();

		for (int i = 0; i < jxPos.Count; i++) {
			posX.Add (double.Parse (jxPos [i].ToString ()));
			posY.Add (double.Parse (jyPos [i].ToString ()));
		}

		InstantiateCustomLevelObjects ();

	}//end load custom level

	//instantiating level objects based upon the input from grid position inputs
	public void InstantiateCustomLevelObjects()
	{
		double lastY = -1f;
		float dist = roadBlockPrefab.GetComponent<BoxCollider> ().size.x * textureToObjectScale;//Debug.Log (dist);
		for (int i = 0; i < posX.Count; i++) {
			GameObject go;
			//road inst
			if(i!=0 && lastY<posY[i])
				go = Instantiate (roadSlantPrefab, new Vector3 (i*dist, (float)posY[i] , 0f), Quaternion.identity) as GameObject;
			else if(i!=0 && lastY>posY[i])
				go = Instantiate (roadSlantPrefab, new Vector3 (i*dist, (float)posY[i-1], 0f), Quaternion.identity) as GameObject;
			else
				go = Instantiate (roadBlockPrefab, new Vector3 (i*dist, (float)posY[i], 0f), Quaternion.identity) as GameObject;

			if (lastY > posY [i])
				go.transform.localScale = new Vector3 (-go.transform.localScale.x * textureToObjectScale, 1, 5f);
			else
				go.transform.localScale = new Vector3 (go.transform.localScale.x * textureToObjectScale, 1, 5f);
			go.transform.parent = customLevelParent.transform;

			//ground inst
			if (i != 0 && lastY > posY [i]) {
				go = Instantiate (groundBlockPrefab, new Vector3 (i * dist, ((float)posY [i - 1] - dist / 2f) / 2f, 0f), Quaternion.identity) as GameObject;
				go.transform.localScale = new Vector3 (go.transform.localScale.x * textureToObjectScale, (float)posY [i-1], 5f);
			} else {
				go = Instantiate (groundBlockPrefab, new Vector3 (i * dist, ((float)posY [i] - dist / 2f) / 2f, 0f), Quaternion.identity) as GameObject;
				go.transform.localScale = new Vector3 (go.transform.localScale.x * textureToObjectScale, (float)posY [i], 5f);
			}
			go.transform.parent = customLevelParent.transform;

			lastY=posY[i];
		}
	}
}//end class custom level handler
