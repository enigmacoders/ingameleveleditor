﻿using UnityEngine;
using System.Collections;

public class LevelCreatorGrid : MonoBehaviour {

	public static LevelCreatorGrid Instance;

	public GameObject gridParent;
	public GameObject gridElement;
	public int rows=2;
	public int columns=2;
	public float size=0.97f;
	public float spaceOffset=0.03f;

	public static GameObject[,] gridArray;

	void Start()
	{
		Instance = this;
		CreateCustomGrid ();
	}

	void CreateCustomGrid()
	{
		gridArray = new GameObject[rows,columns];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				gridArray [i, j] = Instantiate (gridElement, new Vector3 (i*(size + spaceOffset), j*(size + spaceOffset), 0), Quaternion.identity) as GameObject;
				gridArray [i, j].transform.localScale = new Vector3 (size, size, 1f);
				gridArray [i, j].AddComponent<CustomGridElement> ().rowInd=i;
				gridArray [i, j].GetComponent<CustomGridElement> ().colInd=j;
				gridArray [i, j].gameObject.name = "GridElement" + i.ToString () + "_" + j.ToString ();
				gridArray [i, j].transform.parent = gridParent.transform;
			}
		}
	}

	public void ResetGrid()
	{
		for(int i=0;i<gridParent.transform.childCount;i++)
		Destroy (gridParent.transform.GetChild (i).gameObject);

		gridArray = null;

		CreateCustomGrid ();
	}

}
